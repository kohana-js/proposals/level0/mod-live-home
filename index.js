require("@kohanajs/mod-language");
require('kohanajs').addNodeModule(__dirname);

module.exports = {
  ControllerMixinLiveHome: require('./classes/controller-mixin/LiveHome'),
};
