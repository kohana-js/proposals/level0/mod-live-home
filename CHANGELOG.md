# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.1.5](https://gitlab.com/kohana-js/proposals/level0/mod-live-home/compare/v1.1.4...v1.1.5) (2023-03-15)

### [1.1.4](https://gitlab.com/kohana-js/proposals/level0/mod-live-home/compare/v1.1.3...v1.1.4) (2022-03-13)

### 1.1.3 (2022-03-10)

## [1.1.2] - 2021-10-16
### Fixed
- missing payload state.

## [1.1.1] - 2021-10-16
### Added
- autoload ping-update.js

## [1.1.0] - 2021-10-16
### Added
- add payload state to layout

## [1.0.1] - 2021-10-16
### Fixed
- missing config file

## [1.0.0] - 2021-10-12
### Added
- create CHANGELOG.md