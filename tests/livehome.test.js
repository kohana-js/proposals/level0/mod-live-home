const path = require("path");
const { KohanaJS } = require('kohanajs');
const { Controller } = require('@kohanajs/core-mvc');
const ControllerMixinLiveHome = require('../classes/controller-mixin/LiveHome');

KohanaJS.init({ EXE_PATH: `${__dirname}/app`, APP_PATH: `${__dirname}/app` });
KohanaJS.initConfig(new Map([
  ['livehome', require('../config/livehome')],
]));


class C extends Controller.mixin([ControllerMixinLiveHome]) {
  constructor(request) {
    super(request);
  }

  action_test(){
    this.body = 'test bar';
  }

  action_post(){}
  action_countdown(){}
}

describe('Controller Mixin Live Home test', () => {
  test('constructor', async () => {
    const c = new C({ raw: { url: '/articles/recent.aspx' }, body: '' });

    try {
      await c.execute();
      expect('no error').toBe('no error');
    } catch (e) {
      expect('should not run this').toBe('');
    }
  });

  test('config', async () => {
    expect(path.normalize(KohanaJS.config.livehome.ping_file)).toBe(path.normalize(__dirname+'/public/media/ping/update.json'))
    expect(KohanaJS.config.livehome.default_home).toBe('register');
    expect(KohanaJS.config.livehome.url_param_no_redirect).toBe('no-redirect');
  });

  test('post data', async ()=>{
    const c = new C({
      raw: { url: '/articles/recent.aspx?foo=bar', method: 'POST' },
      query: {foo:'bar'},
      body: 'hello=world&tar=sha' }
    );
    await c.execute('post');
    expect(c.status).toBe(200);
    expect(c.state.get('full_action_name')).toBe('action_post');
  });

  test('redirect_home', async () => {
    const c = new C({
      raw: { url: '/'},
    });
    await c.execute();
    if(c.status === 500)console.log(c.error);
    if(c.status === 404)console.log(c.headers);

    expect(c.status).toBe(302);
    expect(c.state.get('full_action_name')).toBe('action_index');
    expect(c.headers.location).toBe('/countdown');
  });

  test('redirect', async () => {
    const c = new C({
      raw: { url: '/articles/recent.aspx?foo=bar'},
      query: {foo:'bar'},
    });
    await c.execute('post');
    if(c.status === 500)console.log(c.error);
    if(c.status === 404)console.log(c.headers);

    expect(c.status).toBe(302);
    expect(c.state.get('full_action_name')).toBe('action_post');
    expect(c.headers.location).toBe('/countdown?foo=bar');
  });

  test('match', async () => {
    const c = new C({
      raw: { url: '/countdown'},
    });
    await c.execute('countdown');
    if(c.status === 500)console.log(c.error);
    if(c.status === 404)console.log(c.headers);

    expect(c.status).toBe(200);
    expect(c.state.get('full_action_name')).toBe('action_countdown');

  });

  test('query param no redirect', async () => {
    const c = new C({
      raw: { url: '/countdown?no-redirect'},
      query: {'no-redirect':''},
    });

    await c.execute('post');
    expect(c.status).toBe(200);
    expect(c.state.get('full_action_name')).toBe('action_post');
  });

  test('query param no redirect 2', async () => {
    const c = new C({
      raw: { url: '/countdown?no-redirect=1'},
      query: {'no-redirect':'1'},
    });

    await c.execute();
    expect(c.status).toBe(200);
    expect(c.state.get('full_action_name')).toBe('action_index');
  });

  test('redirect with language support', async () => {
    const c = new C({
      raw: { url: '/articles/recent.aspx?foo=bar'},
      query: {foo:'bar'},
    });
    c.language = 'en';
    await c.execute('post');
    if(c.status === 500)console.log(c.error);
    if(c.status === 404)console.log(c.headers);

    expect(c.status).toBe(302);
    expect(c.state.get('full_action_name')).toBe('action_post');
    expect(c.headers.location).toBe('/en/countdown?foo=bar');
  })

});
