(fn => (document.readyState !== 'loading') ? fn() : document.addEventListener('DOMContentLoaded', fn))(async ()=>{

  window.onHeartBeatUpdates.push(payload => {
    const currentHome = document.body.getAttribute('data-home');
    const language    = document.body.getAttribute('data-language');

    if(currentHome !== payload.home){
      let target = language ? `/${language}`: '/';
      if(window.location.search) target = target + window.location.search;
      window.location.assign(target);
      return;
    }

    const currentPageState = document.body.getAttribute('data-page-state');
    if(currentPageState !== payload.state){
      document.querySelectorAll('*[data-page-state]').forEach(el => {
        el.setAttribute('data-page-state', payload.state);
      })
    }
  });

});