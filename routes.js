const { RouteList } = require('@kohanajs/mod-route');
const {KohanaJS} = require('kohanajs');

Array.from(KohanaJS.config.livehome.homepages.values()).forEach(it => {
  RouteList.add(`${KohanaJS.config.language.route}/${it}`, 'controller/Home', it);
})

RouteList.add(KohanaJS.config.language.route, 'controller/Home', 'index');